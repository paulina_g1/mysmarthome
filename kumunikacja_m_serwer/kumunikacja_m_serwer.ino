#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <Ethernet.h>

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};

IPAddress ip(192, 168, 0, 2);
int port = 8090;
EthernetServer server(port);
String app_request;
EthernetClient client;


int L_FLOWERS=1;      ///???????????co z tym

RF24 radio(7, 8); // CE, CSN
byte address[][6] = {"1Node","2Node"};

int data_send[5]; //tablica do komunikacji
/* [0] request mode 0-dane do zmiany 1-request o dane z kwiatka -1-water_now
 * [1] dane z kwiatka lub nr kwiatka
 * [2] dane z kwiatka lub tryb
 * [3] dane z kwiatka lub ilosc wody lub poz. wody
 * [4] dane z kwiatka lub co jaki czas
*/

/////////////////////Zapytanie o dane wszytskich kwiatkow/////////////////////////////
void send_request_all(){      
    int   data_received[7*L_FLOWERS];
       for(int i=0; i<7*L_FLOWERS;i++){
  data_received[i]=0;
}

  data_send[0]=1;
  
  radio.stopListening();

  if(!radio.write(data_send, sizeof(data_send))){
    Serial.println("No trasmission");
  }

  radio.startListening();
  unsigned long started_waiting_at = millis();

  while(! radio.available()){
    if(millis() - started_waiting_at >400){
      Serial.println("No response received");
      return;
    }
  }
  radio.read(  data_received, sizeof(  data_received));

      Serial.println("\n received: ");
  for(int i=0; i<7*L_FLOWERS; i++){
    Serial.print(data_received[i]);
    Serial.print(" ");
  }
        
      // Przygtowanie odpowiedzi
      client.println("HTTP/1.1 200 OK");
      client.println("Content-Type: application/json");
      client.println("Access-Control-Allow-Origin:*");
      client.println("");

      // Obiekt odpowiedzi
      client.println("[");
      int j=0;
      for(int i=0; i<L_FLOWERS-1; i++){
        client.println("{");
        client.print("\"number\" :"+String(data_received[j])+",\n");
        client.print("\"mode\" :"+String(data_received[j+1])+",\n");
        client.print("\"lvl_humid\" :"+String(data_received[j+2])+",\n");
        client.print("\"lvl_humid_auto\" :"+String(data_received[j+3])+",\n");
        client.print("\"lvl_water\" :"+String(data_received[j+4])+",\n");
        client.print("\"time_water\" :"+String(data_received[j+5])+",\n");
        client.print("\"last_water\" :"+String(data_received[j+6]));
        client.println("");
        client.println("}");
        client.println(",");
        j=j+7;
      }
      
      client.println("{");
        client.print("\"number\" :"+String(data_received[j])+",\n");
        client.print("\"mode\" :"+String(data_received[j+1])+",\n");
        client.print("\"lvl_humid\" :"+String(data_received[j+2])+",\n");
        client.print("\"lvl_humid_auto\" :"+String(data_received[j+3])+",\n");
        client.print("\"lvl_water\" :"+String(data_received[j+4])+",\n");
        client.print("\"time_water\" :"+String(data_received[j+5])+",\n");
        client.print("\"last_water\" :"+String(data_received[j+6]));
        client.println("");
        client.println("}");
      client.println("]");
//Serial.println("send: ");
//  for(int i=0; i<5; i++){
//    Serial.print(data_send[i]);
//    Serial.print(" ");
//  }

  
}

/////////////////////Zmienianie ustawien danego kwiatka na auto/////////////////////////////
int change_settings_auto(int nr_kwiatka, int lvl_humid){
  data_send[0]=0;
  data_send[1]=1;
  data_send[2]=1;
  data_send[3]=lvl_humid;

    radio.stopListening();

  if(!radio.write(data_send, sizeof(data_send))){
    Serial.println("No trasmission");
    return 0;
  }

Serial.println("send: ");
  for(int i=0; i<5; i++){
    Serial.print(data_send[i]);
    Serial.print(" ");
  }
  radio.startListening();
  return 1;
}
/////////////////////Zmienianie ustawien danego kwiatka na man/////////////////////////////
int change_settings_man(int nr_kwiatka, int lvl_water, int time_water){
  data_send[0]=0;
  data_send[1]=1;
  data_send[2]=0;
  data_send[3]=lvl_water;
  data_send[4]=time_water;

    radio.stopListening();

  if(!radio.write(data_send, sizeof(data_send))){
    Serial.println("No trasmission");
    return 0;
  }

Serial.println("send: ");
  for(int i=0; i<5; i++){
    Serial.print(data_send[i]);
    Serial.print(" ");
  }
  radio.startListening();
  return 1;
}

/////////////////////Podlewanie natychmiast/////////////////////////////
int water_now(int nr){
  data_send[0]=-1;
  data_send[1]=nr;

  radio.stopListening();

  if(!radio.write(data_send, sizeof(data_send))){
    Serial.println("No trasmission");             //zmienić na komunikat do apki
    Serial.println("send: ");
  for(int i=0; i<5; i++){
    Serial.print(data_send[i]);
    Serial.print(" ");
  }
    return 0;
  }

  radio.startListening();
  return 1;
  
}

////////////////////Czy jest alarm? (poz wody)/////////////////////////
int is_alarm(){
  data_send[0]=-2;

    radio.stopListening();

  if(!radio.write(data_send, sizeof(data_send))){
    Serial.println("No trasmission");             //zmienić na komunikat do apki
    return 0;
  }

  radio.startListening();
  
  unsigned long started_waiting_at = millis();

  while(! radio.available()){
    if(millis() - started_waiting_at >400){
      Serial.println("No response received");
      return 0 ;
    }
  }
  int data_received = 0;
  radio.read(  &data_received, sizeof(  int));
        
      // Przygtowanie odpowiedzi
      client.println("HTTP/1.1 200 OK");
      client.println("Content-Type: application/json");
      client.println("Access-Control-Allow-Origin:*");
      client.println("");

      // Obiekt odpowiedzi   
      client.println("{");
        client.print("\"alarm_stat\" :"+String(data_received));
        client.println("");
        client.println("}");
  
  return 1;
}

/////////////////////Setup/////////////////////////////
void setup() {
  Serial.begin(9600);
  radio.begin();
  radio.setPALevel(RF24_PA_MIN);
  radio.setDataRate(RF24_2MBPS);
  radio.setChannel(124);
  radio.openWritingPipe(address[1]);
  radio.openReadingPipe(1, address[0]);
  randomSeed(analogRead(A0));
  Ethernet.begin(mac, ip);
  if (Ethernet.linkStatus() == LinkOFF) {
    Serial.println("Ethernet cable is not connected.");
  }
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
  app_request="";
}
/////////////////////Loop/////////////////////////////
void loop() {
// listen for incoming clients
client = server.available();
  for(int i=0; i<5;i++){
    data_send[i]=0;
  }
if(client){
  while(client.available()){
    app_request = client.readString();
}
Serial.print(app_request);

  if(app_request.indexOf("GET")>=0){ //czy w requestie jest "get"
    if(app_request.indexOf("data")>=0){  //zapytanie do dziecka o dane kwiatków  
     
      send_request_all();  //jako globalna

      delay(1);
      // Zamknięcie łącza
      client.stop();
    }
    else if(app_request.indexOf("alarm")){    //czy jest alarm
      int ok = is_alarm();                //dorobic obsluge bledu
        delay(1);
      // Zamknięcie łącza
      client.stop();
    }
  }
  else if(app_request.indexOf("POST")>=0){
    if(app_request.indexOf("auto")>=0){  //zmiana ustawien na automatyczne
      int nr_flower = app_request[app_request.indexOf("auto")+5] - '0';
      int lvl_humid_1 = app_request[app_request.indexOf("lvl_humid")+12] - '0';
      int lvl_humid_2 = app_request[app_request.indexOf("lvl_humid")+13] - '0';
      int lvl_humid_3 = app_request[app_request.indexOf("lvl_humid")+14] - '0';
      int ok = change_settings_auto(nr_flower, lvl_humid_1*100+lvl_humid_2*10+lvl_humid_3);
      Serial.println(ok);
      if(ok){
      client.println("HTTP/1.1 201 CREATED");
      client.println("Content-Type: application/json");
      client.println("Access-Control-Allow-Origin:*");
      client.println("");

        delay(1);
      // Zamknięcie łącza
      client.stop();
      }
    }
    else if(app_request.indexOf("manual")>=0){
      int nr_flower = app_request[app_request.indexOf("manual")+7] - '0';
      int lvl_water_1 = app_request[app_request.indexOf("lvl_water")+12] - '0';
      int lvl_water_2 = app_request[app_request.indexOf("lvl_water")+13] - '0';
      int lvl_water_3 = app_request[app_request.indexOf("lvl_water")+14] - '0';

      int time_water = app_request[app_request.indexOf("t_water")+10] - '0';    //od 0-9 tryby

      int ok = change_settings_man(nr_flower, lvl_water_1*100+lvl_water_2*10+lvl_water_3, time_water);
      Serial.println(ok);
      if(ok){
      client.println("HTTP/1.1 201 CREATED");
      client.println("Content-Type: application/json");
      client.println("Access-Control-Allow-Origin:*");
      client.println("");

        delay(1);
      // Zamknięcie łącza
      client.stop();
      }    
    }
    else if(app_request.indexOf("w_now")>=0){
      int nr_flower = app_request[app_request.indexOf("w_now")+6] - '0';
      int ok = water_now(nr_flower);
      if(ok){
      client.println("HTTP/1.1 201 CREATED");
      client.println("Content-Type: application/json");
      client.println("Access-Control-Allow-Origin:*");
      client.println("");

        delay(1);
      // Zamknięcie łącza
      client.stop();
      }
    }
  }
  

      
      app_request = ""; //zerowanie stringa przychodzacego
    
     Serial.println("client disconnected");
  }



//delay(1000);
//water_now(1);
//delay(100000000);


 
}
