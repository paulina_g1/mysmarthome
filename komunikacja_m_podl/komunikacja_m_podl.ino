#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

int L_FLOWERS;
int const POMP_F = 20;
RF24 radio(7, 8); // CE, 

byte address[][6] = {"1Node","2Node"};

/////////////////////////struktura kwiatka//////////////////////////
typedef struct flower{         
  int number, lvl_water, time_water;
  int mode; /*  tryb: 1-auto     0-manual */
  int auto_lvl_humid;
/* poziom wilgotnosci jaki ma być ujtrzymany przy trybie automatycznym*/
  int pin_pomp, pin_humid;
  unsigned long previousMillis;        // will store last time LED was updated
  flower* next;

  int get_last_water();       
  int get_humidity();         
  void water_now();           
  void water_manual();        
  void water_auto();          
} flower;

/////////////////////////lista kwiatkow i metody//////////////////////////
 //lista dostępnych kwiatków
flower* head_list_flower = (flower*)malloc(sizeof(flower));
void add_flower();  //dodaje kolejny kwiatek
void del_flower();  //usuwa kwiatek
flower* get_flower();  //zwraca wskaźnik na kwiatek o danym nr

void send_data(int nr_flower);           //

int alarm();                //  

unsigned long currentMillis;


int request_mode[1];
/*1-request o dane kwiatkow    0-dane do zmiany   -1-podlej teraz*/
//int data_flower[4];
/* [0] nr kwaitka
 * [1] poz. wilgotnosci
 * [2] poziom podlewanie(ile wody podawac) i co jaki czas?
 * [3] ostatnie podlewania czas*/
int data_mode[2];
/* [0] nr kwaitka
 * [1] tryb: 1-auto     0-manual */

int data_received[5]; //tablica do komunikacji
/* [0] request mode 0-dane do zmiany 1-request o dane z kwiatka -1-water_now  -2-is_alarm
 * [1] dane z kwiatka lub nr kwiatka
 * [2] dane z kwiatka lub tryb
 * [3] dane z kwiatka lub ilosc wody lub poz. wody
 * [4] dane z kwiatka lub co jaki czas
*/
/////////////////////////obsluga listy kwiatkow//////////////////////////
void add_flower(int pin_p, int pin_h, int num, int lvl_w, int time_w, int m, int lvl_humid){     //dodaje nowy kwiatek
  flower* new_flower = (flower*)malloc(sizeof(flower));
  new_flower->pin_pomp = pin_p;
  new_flower->pin_humid = pin_h;
  new_flower->number = num;
  new_flower->lvl_water = lvl_w;
  new_flower->time_water = time_w;
  new_flower->mode = m;
  new_flower->auto_lvl_humid = lvl_humid;
  new_flower->previousMillis = 0;
  new_flower->next = head_list_flower;
  head_list_flower = new_flower;
  L_FLOWERS++;
}

void del_flower(int nr){        //usuwa kwiatek o danym numerze, czy działa??
  flower* pom = get_flower(nr);//usuwanie z listy
  free(pom); //??
  L_FLOWERS--;
}

flower* get_flower(int nr){         //zwraca wskaznik na kwiatek o danym numerze
  flower* pom = head_list_flower;
//  Serial.println(head_list_flower->number);
  while(pom!=NULL){
    if(pom->number!=nr){
      pom=pom->next;
    }
    else{
  //    Serial.println(head_list_flower->number);
      return pom;  
    }
  }
//  if(head_list_flower->number==nr){
//      return head_list_flower;
//    } else{
//  Serial.println("ojjj");
//  return NULL;
//    }
  
}

/////////////////////////wysyłanie danych wszystkich kwiatkow//////////////////////////
void send_all_flowers(){    //wysyła dane wszystkich kwiatkow
  int data_flowers[7*L_FLOWERS];
  int j=0;
  flower* pom_f = head_list_flower;

  
  for(int i =1; i<=L_FLOWERS; i++){
    pom_f = get_flower(i);
  //  pom_f = head_list_flower;
//      Serial.println(head_list_flower->number);
//  while(pom_f!=NULL){
//    if(pom_f->number!=i){
//      pom_f=pom_f->next;
//    }
//    else{
//      Serial.println(head_list_flower->number);
//      break; 
//    }
//  }
    data_flowers[j+0]=pom_f->number;
    data_flowers[j+1]=pom_f->mode;
    data_flowers[j+2]=pom_f->get_humidity();
    data_flowers[j+3]=pom_f->auto_lvl_humid;
    data_flowers[j+4]=pom_f->lvl_water;
    data_flowers[j+5]=pom_f->time_water;
    data_flowers[j+6]=pom_f->get_last_water();
    j=j+7;
  }
  
  radio.write(data_flowers, sizeof(data_flowers));

    Serial.println("\n send data: ");
     for(int i=0; i<sizeof(data_flowers)/sizeof(int); i++){
    Serial.print(data_flowers[i]);
    Serial.print(" ");
  }
}

/////////////////////////podlewanie natychmiat//////////////////////////
void flower::water_now(){
    digitalWrite(pin_pomp, HIGH);
    delay(lvl_water/POMP_F*1000);
    digitalWrite(pin_pomp, LOW);
}

/////////////////////////podlewanie manualne//////////////////////////
void flower::water_manual(){
 // Serial.println("Uruchamiam water_manual()");
  unsigned long time_water_ms;
  switch(time_water){
    case 1: 
      time_water_ms= 30000;   //test: co 30s
      break;
    case 2: 
      time_water_ms= 86400000;   //codziennie
      break;
    case 3: 
      time_water_ms= 172800000;   //co 2 dni
      break;
    case 4: 
      time_water_ms= 259200000;   //co 3 dni
      break;
  }
  currentMillis = millis();
    if(currentMillis - previousMillis >= time_water_ms){ 
        previousMillis = currentMillis;
        digitalWrite(pin_pomp, HIGH);
        delay(lvl_water/POMP_F*1000);
        digitalWrite(pin_pomp, LOW); 
    }
}

/////////////////////////podlewanie automatyczne//////////////////////////
void flower::water_auto(){
 // Serial.println("Uruchamiam water_auto()");   //dodać opóźnienie o pol h?
    currentMillis = millis();
    if(currentMillis - previousMillis >= 60000){ //sprawdzaj wilgotnosc co minute
        previousMillis = currentMillis;
        if(get_humidity()<=auto_lvl_humid){
          digitalWrite(pin_pomp, HIGH);
          delay(lvl_water/POMP_F*1000);
          digitalWrite(pin_pomp, LOW);
        }  
    }
}

/////////////////////////pobieranie wilgotnosci//////////////////////////
int flower::get_humidity(){
  return analogRead(pin_humid);
}

/////////////////////////kiedy ostatnio podlane//////////////////////////
int flower::get_last_water(){
  Serial.println("Uruchamiam get_last_water()");
  return 99;
}

/////////////////////////alarm o za niskim poz. wody//////////////////////////
int is_alarm(int poz_zbiornik){                              
  if(poz_zbiornik>=900){
    //wyślij ostrzezenie o malym poziomie wody
    return 1;
  } else{
    return 0; 
  }
}

/////////////////////////setup//////////////////////////
void setup() {
  Serial.begin(9600);
  radio.begin();
  radio.setPALevel(RF24_PA_MIN);
  radio.setDataRate(RF24_2MBPS);
  radio.setChannel(124);
  radio.openWritingPipe(address[0]);
  radio.openReadingPipe(1, address[1]);
  radio.startListening();
  L_FLOWERS=0;
  head_list_flower=NULL;
  add_flower(3, 3, 1, 50, 1, 0, 300);
  pinMode(3, OUTPUT);
}

/////////////////////////Loop//////////////////////////
void loop() {
  int poz_zbiornik = analogRead(1);
//unsigned long started_waiting_at = millis();
//  while(! radio.available()){
//    if(millis() - started_waiting_at >400){
//      return;
//    }
//  }

//while(!radio.available()){
//  for(int i=1; i<=L_FLOWERS; i++){
//  if(get_flower(i)->mode==1){ //automatyczny
//    get_flower(i)->water_auto();      
//  }
//  else if (get_flower(i)->mode==0){  //manual
//    get_flower(i)->water_manual();
//  }
//}
//
//}
if(radio.available()){
  Serial.println("ava");
  while(radio.available()){
    radio.read(data_received, sizeof(data_received));
  }

  Serial.println("\n received data: ");
   for(int i=0; i<5; i++){
    Serial.print(data_received[i]);
    Serial.print(" ");
}

  *request_mode=data_received[0];
  if(*request_mode==1){           //wyślij dane wszystkich kwiatkow
    radio.stopListening();
    send_all_flowers();
    radio.startListening();
  }
  else if(*request_mode==0){                      //zmien ustawienia
    data_mode[0]=data_received[1]; //nr kwiatka
    data_mode[1]=data_received[2]; //tryb
    if(data_mode[1]==1){ //automatyczny                
      get_flower(data_mode[0])->mode=1;
      get_flower(data_mode[0])->auto_lvl_humid=data_received[3];
      Serial.println("zmieniam tryb na automatyczny");
    }
    else if(data_mode[1]==0){ //manualny
      get_flower(data_mode[0])->mode=0;
      get_flower(data_mode[0])->lvl_water=data_received[3]; 
      get_flower(data_mode[0])->time_water=data_received[4]; 
      Serial.println("zmieniam tryb na manualny");
    }
  }
  else if(*request_mode==-1){                     //podlej teraz
 //   flower* req_flower = get_flower(data_received[1]);
    //if(req_flower!=NULL){
 //     req_flower->water_now();
   // } //else wysłać błąd???
   // free(req_flower);
   radio.stopListening();
   get_flower(data_received[1])->water_now();
   radio.startListening();
  }
  else if(*request_mode==-2){                     //czy jest alarm
    radio.stopListening();
    int al = is_alarm(poz_zbiornik);
    radio.write(&al, sizeof(int));
    radio.startListening();
  }
}
/////////////////////////////////////////////////////////////////////////
for(int i=1; i<=L_FLOWERS; i++){
  if(get_flower(i)->mode==1){ //automatyczny
    get_flower(i)->water_auto();      
  }
  else if (get_flower(i)->mode==0){  //manual
    get_flower(i)->water_manual();
  }
}





}
