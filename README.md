# MySmartHome
Auto-watering system for Arduino. User can set an automatic mode for watering based on data from humidity sensor or a manual mode - watering every chosen period of time. Setting is available through JSON requests send to server module. 

File komunikacja_m_podl - made for module with Arduino which is resposible for watering and humidity sensor

File komunikacja_m_server - made for module with Arduino with Ethernet shield which is resposible for communication with user and reading requests.

Both modules are communicating through bluetooth and exchanging data. Server sends setting or requests for data to wattering module. Wattering module sends data from sensor and other data to server. 
